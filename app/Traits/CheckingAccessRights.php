<?php

namespace App\Traits;

trait CheckingAccessRights
{
    protected static function hasAccess($user, $access) {
        
        if ($user->roles()->count() > 0) {
            foreach ($user->roles()->get() as $role) {
                if ($role->permissions()->pluck('slug')->contains($access)) {
                    return true;   
                }
            }
        }
        
        return false;
    }
}
