<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();
        
        $model = Permission::class;

        return view('permissions.index', compact('permissions', 'model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('permissions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
        ]);

        try {
            $permission = new Permission();
            $permission->name = $request->input('name');
            $permission->slug = $request->input('slug');
            $permission->save();
        } catch (Exception $ex) {
            return back()->with('status', $e);
        }

        return redirect()->route('permissions');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Permission  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permission = Permission::where('id', $id)->first();

        return view('permissions.edit', compact('permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Permission  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
        ]);

        try {
            $permission = Permission::where('id', $id)->first();
            $permission->name = $request->input('name');
            $permission->slug = $request->input('slug');
            $permission->update();
        } catch (Exception $ex) {
            return back()->with('status', $e);
        }

        return redirect()->route('permissions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Permission  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::where('id', $id)->first();
        
        if ($permission) {
            $permission->delete();
        }
        
        return redirect()->route('permissions');
    }
}
