<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        
        $permissions = Permission::all();
        
        $model = Role::class;

        return view('roles.index', compact('roles', 'permissions', 'model'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();
        
        return view('roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
        ]);
        
        $permission_ids = $request->input('permission_ids');
        
        try {
            $role = new Role();
            $role->name = $request->input('name');
            $role->slug = $request->input('slug');
            $role->save();
        } catch (Exception $ex) {
            return back()->with('status', $e);
        }
        
        $role->permissions()->attach($permission_ids);

        return redirect()->route('roles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::where('id', $id)->first();
        
        $permissions = Permission::all();

        return view('roles.edit', compact('role', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
        ]);
        
        $permission_ids = $request->input('permission_ids');

        try {
            $role = Role::where('id', $id)->first();
            $role->name = $request->input('name');
            $role->slug = $request->input('slug');
            $role->update();
        } catch (Exception $ex) {
            return back()->with('status', $e);
        }
        
        $role->permissions()->sync($permission_ids);
        
        
        return redirect()->route('roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::where('id', $id)->first();
        
        if ($role) {
            $role->delete();
        }
        
        return redirect()->route('roles');
    }
}
