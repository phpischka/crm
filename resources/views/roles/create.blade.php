@extends('layouts.app')

@section('content')
<div class="row">
    @include('layouts.partials.leftmenu')
    <div id="content" class="col-lg-10 col-sm-10">
        <div>
            <ul class="breadcrumb">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Create Role</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-edit"></i> Role Data</h2>

                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        @if (session('status'))
                        <div class="alert alert-info">    
                            {{ session('status') }}    
                        </div>
                        @endif
                        <form role="form" method="post" action="{{route('roleStore')}}">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" placeholder="Enter Name">
                                @error('name')                            
                                    <small class="form-text text-muted">{{ $message }}</small>                        
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <input type="text" name="slug" value="{{old('slug')}}" class="form-control" id="slug" placeholder="Enter Slug">
                                @error('slug')                            
                                    <small class="form-text text-muted">{{ $message }}</small>                        
                                @enderror
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="permission_ids">Permissions</label>
                                <div class="controls">
                                    <select id="permission_ids" name="permission_ids[]" multiple class="form-control" data-rel="chosen">
                                        @foreach($permissions as $permission)
                                            @php
                                                $oldPermissionIds = old('permission_ids') ?? [];
                                            @endphp
                                            @if (in_array($permission->id, $oldPermissionIds))
                                                <option selected value="{{$permission->id}}">
                                                    {{$permission->name}}
                                                </option>
                                            @else
                                                <option value="{{$permission->id}}">
                                                    {{$permission->name}}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-default">Save</button>
                        </form>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
    </div>
</div>
@endsection