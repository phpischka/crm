<!-- left menu starts -->
<div class="col-sm-2 col-lg-2">
    <div class="sidebar-nav">
        <div class="nav-canvas">
            <div class="nav-sm nav nav-stacked">

            </div>
            <ul class="nav nav-pills nav-stacked main-menu">
                <li class="nav-header">Main</li>
                <li>
                    <a class="ajax-link" href="{{route('dashboard')}}">
                        <i class="glyphicon glyphicon-home"></i>
                        <span> Dashboard</span>
                    </a>
                </li>
                @if(Auth::user()->isAdmin(Auth::user()))
                    <li>
                        <a class="ajax-link" href="{{route('users')}}">
                            <i class="glyphicon glyphicon-user"></i>
                            <span> Users</span>
                        </a>
                    </li>
                    <li>
                        <a class="ajax-link" href="{{route('roles')}}">
                            <i class="glyphicon glyphicon-lock"></i>
                            <span> Roles</span>
                        </a>
                    </li>
                    <li>
                        <a class="ajax-link" href="{{route('permissions')}}">
                            <i class="glyphicon glyphicon-lock"></i>
                            <span> Permissions</span>
                        </a>
                    </li>
                @endif
        </div>
    </div>
</div>
<!--/span-->
<!-- left menu ends -->