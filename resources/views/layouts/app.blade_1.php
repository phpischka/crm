<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- The styles -->
        <link id="bs-css" href="{{ asset('css/bootstrap-cybrog.min.css') }}" rel="stylesheet">

        <link href="{{ asset('css/charisma-app.css') }}" rel="stylesheet">
        <link href='{{ asset('bower_components/fullcalendar/dist/fullcalendar.css') }}' rel='stylesheet'>
        <link href='{{ asset('bower_components/fullcalendar/dist/fullcalendar.print.css') }}' rel='stylesheet' media='print'>
        <link href='{{ asset('bower_components/chosen/chosen.min.css') }}' rel='stylesheet'>
        <link href='{{ asset('css/app.css') }}' rel='stylesheet'>
        <link href='{{ asset('bower_components/colorbox/example3/colorbox.css') }}bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
        <link href='{{ asset('bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css') }}' rel='stylesheet'>
        <link href='{{ asset('css/jquery.noty.css') }}' rel='stylesheet'>
        <link href='{{ asset('css/noty_theme_default.css') }}' rel='stylesheet'>
        <link href='{{ asset('css/elfinder.min.css') }}' rel='stylesheet'>
        <link href='{{ asset('css/elfinder.theme.css') }}' rel='stylesheet'>
        <link href='{{ asset('css/jquery.iphone.toggle.css') }}' rel='stylesheet'>
        <link href='{{ asset('css/uploadify.css') }}' rel='stylesheet'>
        <link href='{{ asset('css/animate.min.css') }}' rel='stylesheet'>
        <!-- The fav icon -->
        <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}333
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            @guest
                            @if (Route::has('login'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @endif

                            @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                            @endif
                            @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>

            <main class="py-4">
                @yield('content')
            </main>
        </div>
        <!-- external javascript -->
        <script src="{{ asset('bower_components/jquery/jquery.min.js') }}"></script>

        <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

        <!-- library for cookie management -->
        <script src="{{ asset('js/jquery.cookie.js') }}"></script>
        <!-- calender plugin -->
        <script src='{{ asset('bower_components/moment/min/moment.min.js') }}'></script>
        <script src='{{ asset('bower_components/fullcalendar/dist/fullcalendar.min.js') }}'></script>
        <!-- data table plugin -->
        <script src='{{ asset('js/jquery.dataTables.min.js') }}'></script>

        <!-- select or dropdown enhancer -->
        <script src="{{ asset('bower_components/chosen/chosen.jquery.min.js') }}"></script>
        <!-- plugin for gallery image view -->
        <script src="{{ asset('bower_components/colorbox/jquery.colorbox-min.js') }}"></script>
        <!-- notification plugin -->
        <script src="{{ asset('js/jquery.noty.js') }}"></script>
        <!-- library for making tables responsive -->
        <script src="{{ asset('bower_components/responsive-tables/responsive-tables.js') }}"></script>
        <!-- tour plugin -->
        <script src="{{ asset('bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js') }}"></script>
        <!-- star rating plugin -->
        <script src="{{ asset('js/jquery.raty.min.js') }}"></script>
        <!-- for iOS style toggle switch -->
        <script src="{{ asset('js/jquery.iphone.toggle.js') }}"></script>
        <!-- autogrowing textarea plugin -->
        <script src="{{ asset('js/jquery.autogrow-textarea.js') }}"></script>
        <!-- multiple file upload plugin -->
        <script src="{{ asset('js/jquery.uploadify-3.1.min.js') }}"></script>
        <!-- history.js for cross-browser state change on ajax -->
        <script src="{{ asset('js/jquery.history.js') }}"></script>
        <!-- application script for Charisma demo -->
        <script src="{{ asset('js/charisma.js') }}"></script>
    </body>
</html>
