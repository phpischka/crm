<?php

define("FACEBOOK_APP_ID", "1075355016331211");
define("FACEBOOK_APP_SECRET", "2f8ede11de0c7799affb58a18e2d44e5");
define("FACEBOOK_REDIRECT_URI", "https://crm.novikspecteh.dp.ua/instagram_graph_api/access_token");

//facebook credentials

$creds = array(
    'app_id' => FACEBOOK_APP_ID,
    'app_secret' => FACEBOOK_APP_ID,
    'default_graph_version' => 'v11.0',
        //'persistent_data_handler' => 'session',
);

//fb object
$fb = new \Facebook\Facebook($creds);

$helper = $fb->getRedirectLoginHelper();

$oAuth2Client = $fb->getOAuth2Client();

if (filter_input(INPUT_GET, 'code')) { //get access token
    try {
        $accessToken = $helper->getAccessToken();
    } catch (\Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch (\Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }
    if (!$accessToken->isLongLived()) {
        try {
            $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Error getting long lived accessToken: ' . $e->getMessage();
        }
    }
    var_dump($accessToken);
} else {
    $permissions = ['public_profile', 'instagram_basic', 'pages_show_list'];
    $loginUrl = $helper->getLoginUrl(FACEBOOK_REDIRECT_URI, $permissions);

    echo '<a href="' . $loginUrl . '">Login with facebook</a>';
}