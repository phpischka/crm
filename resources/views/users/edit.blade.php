@extends('layouts.app')

@section('content')
<div class="row">
    @include('layouts.partials.leftmenu')
    <div id="content" class="col-lg-10 col-sm-10">
        <div>
            <ul class="breadcrumb">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Edit User</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-edit"></i> User Data</h2>

                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        @if (session('status'))
                            <div class="alert alert-info">    
                                {{ session('status') }}    
                            </div>
                        @endif
                        <form role="form" method="post" action="{{url('users/' . $user->id . '/update')}}">
                            @csrf
                            <div class="control-group">
                                <label class="control-label" for="role_ids">Roles</label>
                                <div class="controls">
                                    <select id="role_ids" name="role_ids[]" multiple class="form-control" data-rel="chosen">
                                        @foreach($roles as $role)
                                            @if ($user->roles()->find($role->id))
                                                <option selected value="{{$role->id}}">
                                                    {{$role->name}}
                                                </option>
                                            @else
                                                <option value="{{$role->id}}">
                                                    {{$role->name}}
                                                </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                @error('role_ids')                            
                                    <small class="form-text text-muted">{{ $message }}</small>                        
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" value="{{$user->name}}" class="form-control" id="name" placeholder="Enter Name">
                                @error('name')                            
                                    <small class="form-text text-muted">{{ $message }}</small>                        
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" name="email" value="{{$user->email}}" class="form-control" id="email" placeholder="Enter email">
                                @error('email')                            
                                    <small class="form-text text-muted">{{ $message }}</small>                        
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                                @error('password')                            
                                    <small class="form-text text-muted">{{ $message }}</small>                        
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">Password Confirm</label>
                                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password Confirm">
                                @error('password_confirmation')                            
                                    <small class="form-text text-muted">{{ $message }}</small>                        
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-default">Save</button>
                        </form>

                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
    </div>
</div>
@endsection