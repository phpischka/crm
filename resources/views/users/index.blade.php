@extends('layouts.app')

@section('content')
<div class="row">

    @include('layouts.partials.leftmenu')

    <noscript>
    <div class="alert alert-block col-md-12">
        <h4 class="alert-heading">Warning!</h4>

        <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
            enabled to use this site.</p>
    </div>
    </noscript>

    <div id="content" class="col-lg-10 col-sm-10">
        <!-- content starts -->
        <div>
            <ul class="breadcrumb">
                <li>
                    <a href="{{route('dashboard')}}">Home</a>
                </li>
                <li>
                    <a href="#">Users</a>
                </li>
            </ul>
        </div>
        @can('create', $model)
            <a class="btn btn-success " href="{{route('userCreate')}}">
                <i class="glyphicon glyphicon-plus icon-white"></i>
                Add User
            </a>
        @endcan
        <div class="row">
            <div class="box col-md-12">

                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-user"></i> Users</h2>

                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        @if (session('status'))
                            <div class="alert alert-info">    
                                {{ session('status') }}    
                            </div>
                        @endif
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Roles</th>
                                    @can('update', $model)
                                        <th>Actions</th>
                                    @endcan
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td class="center">{{$user->email}}</td>
                                        <td class="center">
                                            @if($user->roles()->count() > 0)
                                                @foreach($user->roles()->get() as $role)
                                                    <p>{{$role->name}}</p>
                                                @endforeach
                                            @endif
                                        </td>
                                        @can('update', $model)
                                            <td class="center">
                                                @can('update', $model)
                                                    <a class="btn btn-info" href="{{url('/users/' . $user->id . '/edit')}}">
                                                        <i class="glyphicon glyphicon-edit icon-white"></i>
                                                        Edit
                                                    </a>
                                                @endcan
                                                @can('delete', $model)
                                                    <a class="btn btn-danger" href="{{url('/users/' . $user->id . '/delete')}}">
                                                        <i class="glyphicon glyphicon-trash icon-white"></i>
                                                        Delete
                                                    </a>
                                                @endcan
                                            </td>
                                        @endcan
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--/span-->

        </div><!--/row-->

        <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->

@endsection
