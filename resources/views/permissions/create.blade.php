@extends('layouts.app')

@section('content')
<div class="row">
    @include('layouts.partials.leftmenu')
    <div id="content" class="col-lg-10 col-sm-10">
        <div>
            <ul class="breadcrumb">
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">Create Permission</a>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-edit"></i> Permission Data</h2>

                        <div class="box-icon">
                            <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                            <a href="#" class="btn btn-minimize btn-round btn-default"><i class="glyphicon glyphicon-chevron-up"></i></a>
                            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>
                    </div>
                    <div class="box-content">
                        @if (session('status'))
                            <div class="alert alert-info">    
                                {{ session('status') }}    
                            </div>
                        @endif
                        <form role="form" method="post" action="{{route('permissionStore')}}">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" value="{{old('name')}}" class="form-control" id="name" placeholder="Enter Name">
                                @error('name')                            
                                    <small class="form-text text-muted">{{ $message }}</small>                        
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <input type="text" name="slug" value="{{old('slug')}}" class="form-control" id="slug" placeholder="Enter Slug">
                                @error('slug')                            
                                    <small class="form-text text-muted">{{ $message }}</small>                        
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-default">Save</button>
                        </form>

                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
    </div>
</div>
@endsection