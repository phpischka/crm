<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\FacebookController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {
   //dashboard
   Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
   //users
   Route::get('/users', [UserController::class, 'index'])
           ->name('users')
           ->middleware('can:view,App\Models\User');
   Route::get('/users/create', [UserController::class, 'create'])
           ->name('userCreate')
           ->middleware('can:create,App\Models\User');
   Route::post('/users/store', [UserController::class, 'store'])
           ->name('userStore')
           ->middleware('can:create,App\Models\User');
   Route::get('/users/{id}/edit', [UserController::class, 'edit'])
           ->middleware('can:update,App\Models\User');
   Route::post('/users/{id}/update', [UserController::class, 'update'])
           ->middleware('can:update,App\Models\User');
   Route::get('/users/{id}/delete', [UserController::class, 'destroy'])
           ->middleware('can:delete,App\Models\User');
   //roles
   Route::get('/roles', [RoleController::class, 'index'])
           ->name('roles')
           ->middleware('can:view,App\Models\Role');
   Route::get('/roles/create', [RoleController::class, 'create'])
           ->name('roleCreate')
           ->middleware('can:create,App\Models\Role');
   Route::post('/roles/store', [RoleController::class, 'store'])
           ->name('roleStore')
           ->middleware('can:create,App\Models\Role');
   Route::get('/roles/{id}/edit', [RoleController::class, 'edit'])
           ->middleware('can:update,App\Models\Role');
   Route::post('/roles/{id}/update', [RoleController::class, 'update'])
           ->middleware('can:update,App\Models\Role');
   Route::get('/roles/{id}/delete', [RoleController::class, 'destroy'])
           ->middleware('can:delete,App\Models\Role');
   //permissions
   Route::get('/permissions', [PermissionController::class, 'index'])
           ->name('permissions')
           ->middleware('can:view,App\Models\Permission');
   Route::get('/permissions/create', [PermissionController::class, 'create'])
           ->name('permissionCreate')
           ->middleware('can:create,App\Models\Permission');
   Route::post('/permissions/store', [PermissionController::class, 'store'])
           ->name('permissionStore')
           ->middleware('can:create,App\Models\Permission');
   Route::get('/permissions/{id}/edit', [PermissionController::class, 'edit'])
           ->middleware('can:update,App\Models\Permission');
   Route::post('/permissions/{id}/update', [PermissionController::class, 'update'])
           ->middleware('can:update,App\Models\Permission');
   Route::get('/permissions/{id}/delete', [PermissionController::class, 'destroy'])
           ->middleware('can:delete,App\Models\Permission');
   
   Route::get('/instagram_graph_api/access_token', [FacebookController::class, 'access_token']);
});


